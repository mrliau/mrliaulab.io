---
layout: page
title: 日本語 Students
permalink: /students/
---
<img src="/assets/dangerous.jpeg" alt="Source: Kotaku" width="500" height="281" /> 
<br/>Source: Kotaku

A collection of resources for vocabulary building.

<strong>Sensei's Quizlet Page</strong>
<a href="https://quizlet.com/liausensei">https://quizlet.com/liausensei</a>

<strong>KANJI Learners</strong>
<ul>
 	<li><a href="https://quizlet.com/141506293/common-kanji-combo-flash-cards/">Kanji Combo Test Study</a></li>
 	<li><a href="http://japanese.about.com/od/kan2/a/100kanji.htm">Top 100 Most Common Kanji Part 1</a> / <a href="https://quizlet.com/10093764/100-most-common-kanji-flash-cards/">Quizlet Version</a></li>
 	<li><a href="http://japanese.about.com/od/kan2/a/100kanji2.htm">Top 100 Most Common Kanji Part 2</a></li>
 	<li><a href="http://www.hellodamage.com/tdr/archive/7diary/byfreq.html">2501 most frequent kanji</a></li>
 	<li><a href="http://japanese-lesson.com/characters/kanji/kanji_writing.html">Kanji Writing Practice (Print Me)</a></li>
 	<li><a href="https://addons.mozilla.org/en-US/firefox/addon/rikaichan/">Read Any Kanji in your Browser- Rikaichan</a></li>
</ul>
<strong>KANA Learners</strong>
<ul>
 	<li><a href="http://japanese-lesson.com/characters/hiragana/hiragana_writing.html">Hiragana Writing Practice (Print Me)</a></li>
 	<li><a href="http://japanese-lesson.com/characters/katakana/katakana_writing.html">Katakana Writing Practice (Print Me)</a></li>
 	<li><a href="https://quizlet.com/24255617/hiragana-12-all-46-hiragana-practice-flash-cards/">Quizlet</a></li>
 	<li><a href="http://japanese.gatech.edu/WebCTVista/JAPN1001/contents/Lesson02/hiragana/mnemonic-hiragana.html">Hiragana Mnemonics</a></li>
<li><a href="http://japanese.gatech.edu/WebCTVista/JAPN1001/contents/Lesson02/katakana/mnemonic-katakana.html">Katakana Mnemonics</a></li>
</ul>
<strong>VERB Learners</strong>
<ul>
<li><a hreaf="http://www.japaneseverbconjugator.com/">Verb Conjugator (Online)</a></li>
</ul>
<strong>ADJECTIVE Learners</strong>
<ul>
 	<li><a href="http://japanese.about.com/od/grammarlessons/a/Useful-Japanese-Adjectives.htm">Useful Adjectives</a></li>
 	<li><a href="http://www.punipunijapan.com/japanese-colors-%E8%89%B2/">Colours</a></li>
</ul>
<strong>DAILY VOCABULARY</strong>
<ul>
 	<li><a href="http://liau.ca/school/wp-content/uploads/2015/11/Day-counting.pdf" rel="">Day Counting (Print Me)</a></li>
</ul>
<strong>DICTIONARY</strong>
<ul>
 	<li><a href="http://rut.org/cgi-bin/j-e/sjis/tty/dict">和英／英和辞典 - Japanese⇔English Dictionary</a></li>
<li><a href="http://jisho.org/">jisho.org</a></li>
</ul>
&nbsp;

&nbsp;
