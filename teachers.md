---
layout: page
title: Teachers
permalink: /teachers/
---
<strong>Getting Started with TPRS for Japanese</strong>

Get involved, get educated, and practice!

There are many TPRS communities online, such as <a href="https://tprsquestionsandanswers.wordpress.com/">TPRS Q&A</a>. Most TPRS teachers are helpful and excited to share their knowledge. 

This book was recommended to me, and I will be using the techniques in it moving forward: <a href="http://terrywaltz.com/the-red-book-not-just-for-chinese/">TPRS with Chinese Characteristics</a> by Terry Waltz. YES, it also applies to Japanese. 


<strong>Japanese Word Frequency</strong>

TPRS teaching is based on word frequency.

TPRS Japanese Word Frequency lists:

<a href="http://www.manythings.org/japanese/words/leeds/">ManyThings</a>

<a href="https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/Japanese">Wiktionary</a>

<a href="http://www.lexiteria.com/word_frequency/japanese_word_frequency_list.html">Lexiteria</a>

<strong>What is TPRS?</strong>

Teaching Proficiency [through] Reading [and] Storytelling
<a href="https://en.wikipedia.org/wiki/TPR_Storytelling">TPRS Wikipedia</a>

TPRS is used for teaching/learning foreign languages. It is a comprehensible-input method.
Who does TPRS? 

TPRS is becoming more popular in the public high school system. Ask your teachers and tutors to look into this method.

<strong>Why TPRS?</strong>

It is an effective face-to-face method of learning and a better use of time than doing grammar worksheets. Although it requires more interaction, it can have a lot less preparation.

TPRS is designed for differentiated learning and can work in multi-level classrooms.

<strong>When should I use TPRS?</strong>

Any time you want to teach language acquisition. It can replace an entire curriculum. Some people prefer to mix in traditional methods of lecture, while others believe TPRS is the only good use of class time. Keep in mind, that reading is a big part of TPRS.

<strong>How do I do TPRS?</strong>

TPRS requires practice. Training is also recommended. You can attend a <a href="https://tprsbooks.com/tprs-workshops/">Blaine Ray workshop.</a>

TPRS workshops are often at professional development conferences.


