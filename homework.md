---
layout: default
title: Homework
permalink: /homework/
---

<div class="home">

  <h1 class="page-heading">Posts</h1>

  <ul class="post-list">
    {% for post in site.categories.homework limit:5 %}
      <li>
            <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
      </li>
    {% endfor %}
  </ul>

  <p> <a href="{{ site.baseurl }}/homework-archive.html">Homework Archive</a></p>
  <p class="rss-subscribe">subscribe to homework <a href="{{ "/homework.xml" | prepend: site.baseurl }}">via RSS</a></p>

</div>


