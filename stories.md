---
layout: default
title: Stories
permalink: /stories/
---

<div class="home">

  <h1 class="page-heading">Posts</h1>

  <ul class="post-list">
    {% for post in site.posts %}
      {% if post.categories contains 'Classroom' %}
      <li>
        <span class="post-meta">{{ post.date | date: "%Y-%m-%d" }}</span>
        <div class="front-post" style="{% if post.featureimage %}background-image: url('{{ post.featureimage }}');{% else %}background-image: url('/images/defaultfeature.jpg');{% endif %}">
          <div class="front-title">
            <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
            {{ post.summary }}
          </div>

        </div>
      </li>
      {% endif %}
    {% endfor %}
  </ul>

  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>

</div>



