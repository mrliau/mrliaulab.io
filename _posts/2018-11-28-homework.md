---
layout: homework
title: Homework 2018-11-28
date: 2018-11-28 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
<b>Burnaby Mountain</b>

Happening on 2018-12-03
- Japanese 12 - Written assessment
- Japanese 11 - Comic Due, reading assessment ([Lacoon](http://mrliau.com/classroom/2018/11/27/arbeit.html))
- Japanese 10 - Comic Due, reading assessment ([Shakira](http://mrliau.com/classroom/2018/11/27/the-animal.html))
- Japanese Intro 11 -  Comic Due, reading assessment ([Shakira](http://mrliau.com/classroom/2018/11/27/the-animal.html))
- Japanese 9 -  Comic Due, reading assessment ([Chicken](http://mrliau.com/classroom/2018/11/27/the-animal.html))


<b>Burnaby South</b>

Happening on 2018-11-30
- Japanese 11 - Comic Due, big kanji test ([Squirrel](http://mrliau.com/classroom/2018/11/27/arbeit.html))
- Japanese 10 - Comic Due, big hiragana katakana test ([Squirrel](http://mrliau.com/classroom/2018/11/27/arbeit.html))
- Japanese Intro 11 -  Comic Due, big hiragana/katakana test ([Shrek/Turtle](http://mrliau.com/classroom/2018/11/27/the-animal.html))
- Japanese 9 -  Comic Due, big hiragana test ([Shrek/Turtle](http://mrliau.com/classroom/2018/11/27/the-animal.html))

Happening on 2018-12-04
- Japanese 12 - Written assessment
- Japanese 11 - reading assessment ([Squirrel](http://mrliau.com/classroom/2018/11/27/arbeit.html))
- Japanese 10 - reading assessment ([Squirrel](http://mrliau.com/classroom/2018/11/27/the-animal.html))
- Japanese Intro 11 - reading assessment ([Shrek/Turtle](http://mrliau.com/classroom/2018/11/27/the-animal.html))
- Japanese 9 - reading assessment ([Shrek/Turtle](http://mrliau.com/classroom/2018/11/27/the-animal.html))

EXTRA READING PRACTICE:
- www.textivate.com
- username: liausensei
- student password: student7454
- Click magnifying glass: search Public for "liausensei"
- Look for the story that you are doing.