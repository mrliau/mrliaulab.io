---
layout: homework
title: Homework 2017-11-20
date: 2017-11-20 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class
- Click on the magnifying glass
- Search "Public" resources for "liausensei"

<b>Burnaby Mountain</b>

Happening on 2017-11-17
- Japanese 9/Intro 11 - hiragana katakana quiz ma mi mu me mo

Happening on 2017-11-21
- Japanese 12 - Work on Presentations
- Japanese 11 - Work
- Japanese 10 - da ji dzu de do , ba bi bu be bo, pa pi pu pe po
- Japanese 9/11 intro - TBD

Happening on 2017-11-23
- Japanese 12 - Presentations / Test
- Japanese 11 - Comic and review
- Japanese 10 - Comic and review
- Japanese 9/11 intro - Comic and review

-- Comic: major plot points, manga style (right to left, ADD adjectives, dialogue), roughly 6+ panels, shading

<b>Burnaby South</b>

Happening on 2017-11-20
- Japanese 9/10/11 intro hiragana katakana quiz - ma mi mu me mo

Happening on 2017-11-22
- Japanese 12 - Presentation PRACTICE (for the 27th)
- Japanese 11:
<br />-- kanji learning 手　市　見　米　力
<br />-- Comic: major plot points, manga style (right to left, ADD adjectives, dialogue), roughly 6+ panels, shading
- Japanese 9/10/11 intro:
<br />-- Comic: major plot points, manga style (right to left, ADD adjectives, dialogue), roughly 6+ panels, shading
<br />-- Quiz: ya yu yo wa wo n

