---
layout: homework
title: Homework 2017-10-27
date: 2017-10-27 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class
- Click on the magnifying glass
- Search "Public" resources for "liausensei"


<b>Burnaby Mountain</b>

Happening on 2017-11-01
- Japanese 12 - Kanji Quiz
- Japanese 10 - Katakana Quiz ra ri ru re ro wa wo n
- Japanese 11 - Kanji Quiz 同　今　高　金　時
- Japanese 9/Intro 11 - hiragana katakana quiz na ni nu ne no

<b>Burnaby South</b>

Happening on 2017-11-02
- Japanese 12 - Kanji Quiz
- Japanese 11 - kanji quiz 子　東　三　分　行
- Japanese 9/10/11 intro - hiragana katakana quiz na ni nu ne no
