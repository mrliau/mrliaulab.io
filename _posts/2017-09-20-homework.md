---
layout: homework
title: Homework 2017-09-20
date: 2017-09-20 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---

<b>Burnaby Mountain</b>

2017-09-27 - Wednesday
- Japanese 12 - Kanji Quiz, Genki Ch 4
- Japanese 10/11 - Kanji, 100 most common, first 5 (two readings)
- Japanese 9/Intro 11 - ka ki ku ke ko quiz, Find Anime

<b>Burnaby South</b>

2017-09-26 - Tuesday
- Japanese 12 - Kanji Quiz, Genki Ch 4
- Japanese 11 - Kanji, 100 most common, first 5 (two readings)
- Japanese 9/10/11 intro - a i u e o quiz

