---
layout: homework
title: Homework 2017-12-08
date: 2017-12-08 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class: liausensei/student7454
- Click on the magnifying glass
- Search "Public" resources for "liausensei"

<b>Burnaby Mountain</b>

Happening on 2017-12-08

- Japanese 9/11 intro - ga gi gu ge go za ji zu ze zo

Happening on 2017-12-12

- Japanese 9/11 intro - da ji* dzu de do

<b>Burnaby South</b>

Happening on 2017-12-11

- Japanese 9/10/11 intro: ga gi gu ge go za ji zu ze zo

Happening on 2017-12-15

- Japanese 12 - Vocab Quiz
- Japanese 11 - Kanji Quiz　自　合　前　立　円
- Japanese 9/10/11 intro: da ji* dzu de do

