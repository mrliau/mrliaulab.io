---
layout: homework
title: Listen to This - Stay Focused
date: 2017-10-14 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---

<a href="http://jimkwik.com/kwik-brain-040/">Kwik Brain 040: How To Stay Focused (And Fix A Wandering Mind)</a>

Jim Kwik (Yes, that's his real name), pro speed-reader, tells us how to improve our focus and productivity for work, studying, or any task. 

PRO TIP: Sleeping is a task.


