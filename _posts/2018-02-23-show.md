---
layout: homework
title: Listen to This - Samurai Crabs, Unlucky Days, Social Media Against Humanity
date: 2018-02-23 01:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---

<a href="https://www.stufftoblowyourmind.com/podcasts/sagan-samurai-crabs.htm">Stuff to Blow Your Mind - Carl Sagan and the Samurai Crabs</a>

Are crabs actually people? Are people crabs? Find out why the faces of samurai are on the backs of these real crabs.

<a href="http://www.uncannyjapan.com/episode-15-inviting-friend-die-rokuyo/">Uncanny Japan - Rokuyo 六曜</a>

Have you ever wondered what those kanji mean on Japanese calendar days? Maybe you have never even noticed, or seen them. Find out when is the best time to have a wedding or have a funeral. If only we could really plan theses things...

<a href="http://teamhuman.fm/episodes/ep-74-damien-williams/">Team Human - We Built it From Us</a>

Program or be programmed. If you don't change society, then society will change you. Find out why social media has no place in a school classroom. The reasons may surprise you!





