---
layout: homework
title: Homework 2018-10-11
date: 2018-10-11 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
<b>Burnaby Mountain</b>

Happening on 2018-10-15
- Japanese 12 - A quiz - Conversation about self
- Japanese 11 - Quiz - 会　月　人　長　本
- Japanese 10 - Quiz - ra ri ru re ro wa wo n writing
- Japanese Intro 11 - Quiz hiragana katakana na ni nu ne no reading ; hand in writing practice
- Japanese 9 - Quiz Hiragana na ni nu ne no ; hand in writing practice


<b>Burnaby South</b>

Happening on 2018-10-12
- Japanese 11 - Quiz - 会　月　人　長　本

Happening on 2018-10-16
- Japanese 12 - A quiz - Conversation about self
- Japanese 10 - Quiz - ra ri ru re ro wa wo n writing
- Japanese Intro 11 - Quiz hiragana katakana na ni nu ne no reading ; hand in writing practice
- Japanese 9 - Quiz Hiragana na ni nu ne no ; hand in writing practice


EXTRA READING PRACTICE:
- www.textivate.com
- username: liausensei
- student password: student7454
- Click magnifying glass: search Public for "liausensei"
- Look for the story that you are doing.