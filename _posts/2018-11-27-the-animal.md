---
layout: post
title: TPRS Japanese Story - The Animals (Shrek, Turtle, Chicken, Shakira)
date: 2018-11-27 00:51:36.000000000 -08:00
type: post
published: true
status: publish
categories:
- Classroom
tags:
- japanese
- learn japanese
- tprs
- tprs japanese
- tprs story
meta:
  _edit_last: '1'
  _sq_fp_title: ''
  _sq_fp_description: ''
  _sq_canonical: ''
  _sq_post_keyword: '{"keyword":"japanese","update":1493784955,"rank":-1,"country":"com","language":"en"}'
  _thumbnail_id: '254'
author: Joseph Liau
summary: Animals have problems too. Can we solve them with our human ways?
featureimage: /assets/2018-2019/theanimal.jpg
---
TPRS Level - Year 1/Year2

[View/Download Documents](https://sd41-my.sharepoint.com/:f:/g/personal/e18727_burnabyschools_ca/EsGut0_sjfNDlbJa5EYDV2wBVtVhV8j1X5Gr3hnCZvabQw?e=b5hJl8)

Textivate:
- [TPRS Japanese Story: Chicken](https://www.textivate.com/vdskn1)
- [TPRS Japanese Story: Shrek](https://www.textivate.com/99skn1)
- [TPRS Japanese Story: Turtle](https://www.textivate.com/saskn1)
- [TPRS Japanese Story: Shakira (Grade 10)](https://www.textivate.com/ydskn1)

user: liausensei
student password: student7454

Structures Used:
- Past tense (end with た）
- Negatives (ない）
- Is (だ）
- Is there (いる）
- Likes/dislikes (すき）
- There is / has (いる/ある）
- Wants (ほしい）
- Wants to do (～たい）
- The most (いちばん）
- Good at (じょうず)

