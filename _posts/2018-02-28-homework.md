---
layout: homework
title: Homework 2018-02-28
date: 2018-02-28 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class: liausensei/student7454
- Click on the magnifying glass
- Search "Public" resources for "liausensei"

<b>Burnaby Mountain</b>

Happening on 2018-03-02

- Japanese 12 - 
- Japanese 11 - Kanji quiz 女、方、部、明、新
- Japanese 10 -　Kanji quiz 思う、言う、年、中、見る
- Japanese 9/11 intro - Quiz mya myu myo, rya ryu ryo

<b>Burnaby South</b>

Happening on 2018-03-01

- Japanese 12 - Project Due
- Japanese 11 - Project Due
- Japanese 9/10/11 intro - Mya myu myo rya ryu ryu test, quick write in class

Happening on 2018-03-05

- Japanese 12 - Some Quiz
- Japanese 11 - Kanji Quiz 女、方、部、明、新
- Japanese 9/10/11 intro - quick write in class
