---
layout: homework
title: Homework 2017-10-18
date: 2017-10-13 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class
- Click on the magnifying glass
- Search "Public" resources for "liausensei"


<b>Burnaby Mountain</b>

Happening on 2017-10-24
- Japanese 12 - te-form quiz
- Japanese 10 - Katakana quiz ma mi mu me mo ya yu yo
- Japanese 11 - Kanji quiz 子　東　三　分　行
- Japanese 9/Intro 11 - Hiragana Katakana in-class worksheet あ〜と, ア〜ﾄ

<b>Burnaby South</b>

Happening on 2017-10-19
- Japanese 12 - Interview Test
- Japanese 11 - Kanji Quiz? 国　出　上　十　生
- Japanese 9/10/11 intro - Review Kana from "A" until "TO" 
