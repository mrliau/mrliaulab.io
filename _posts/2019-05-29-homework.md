---
layout: homework
title: Homework 2019-05-29
date: 2019-05-29 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
<b>Burnaby South</b>

Happening 2019-05-31
* Japanese 12: Project Presentation
* Japanese 11: Comic / Practice
* Japanese 10: Comic / Practice
* Japanese 9/Intro: Kanji quiz 上、下、中、半、日本人

<b>Burnaby Mountain</b>

Happening 2019-06-03
* Japanese 12: Project Presentation
* Japanese 11: Picture Practice
* Japanese 10: Comic / Practice
* Japanese 9/Intro: Kanji quiz 上、下、中、半、日本人