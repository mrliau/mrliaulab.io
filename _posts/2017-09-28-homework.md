---
layout: homework
title: Homework 2017-09-28
date: 2017-09-28 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---

<b>Burnaby Mountain</b>

2017-10-05
- Japanese 12 - Vocab Quiz Genki Ch 4 , Self-Description Project
- Japanese 10 - Katakana, sa shi su se so ta chi tsu te to
- Japanese 11 - Kanji, 100 most common, first 10 会　本　月　長　人
- Japanese 9/Intro 11 - sa shi su se so quiz, Find Anime

<b>Burnaby South</b>

2017-10-04 
- Japanese 12 - Kanji Quiz Genki Ch 4, Self-Description Project
- Japanese 11 - Kanji, 100 most common, first 10 会　本　月　長　人
- Japanese 9/10/11 intro - sa shi su se so quiz, Find Anime
