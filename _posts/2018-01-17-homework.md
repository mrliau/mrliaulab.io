---
layout: homework
title: Homework 2018-01-17
date: 2018-01-17 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
EXTRA PRACTICE

CHECK OUT <a href="http://www.textivate.com">www.textivate.com</a>
- Login with the student login given in class: liausensei/student7454
- Click on the magnifying glass
- Search "Public" resources for "liausensei"

<b>Burnaby Mountain</b>

Happening on 2018-01-19

- Japanese 12 - Genki I - Chapter 8 Dialogue Overview
- Japanese 11 - Dialogue Practice 、kanji　田　地　京　間　体
- Japanese 10 - Dialogue Practice
- Japanese 9/11 intro - sha shi shu she sho ja ji ju je jo + ANIME SHARE ASSIGNMENT

Happening on 2018-01-23
- Japanese 12 - Genki I Ch8 Dialogue Quiz
- Japanese 11 - Kanji quiz 田　地　京　間　体
- Japanese 10 - Days of the week kanji
- Japanese intro 11 - Days of the week kanji


<b>Burnaby South</b>

Happening on 2018-01-18

- Japanese 12 - Genki I - Chapter 8 Dialogue Overview
- Japanese 11 - Dialogue Practice　体
- Japanese 9/10/11 intro - ANIME SHARE ASSIGNMENT

Happening on 2018-01-22

- Japanese 11 Kanji - 田　地　京　間　体
- Japanese 9/10/11 intro - sha shi shu she sho ja ji ju je jo 

Happening on 2018-01-24

- Japanese 12 - Ch8 Dialogue Quiz
- Japanese 10/intro 11 - days of the week kanji
- Japanese 9 - days of the week hiragana?



