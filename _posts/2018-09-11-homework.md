---
layout: homework
title: Homework 2018-09-11
date: 2018-09-11 00:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---
<b>Burnaby Mountain</b>

Happening on 2018-09-17
- Japanese 12 - Te-form quiz!
- Japanese 10 - Quiz Katakana Quiz a i u e o / ka ki ku ke ko writing
- Japanese Intro 11 - Quiz Hiragana/Katakana a i u e o reading 
- Japanese 11 - Quiz All/any hiragana writing/reading
- Japanese 9 - Quiz Hiragana a i u e o reading

<b>Burnaby South</b>

Happening on 2018-09-14
- Japanese 12 - Quiz Te-form quiz!
- Japanese 10 - Quiz Katakana Quiz a i u e o / ka ki ku ke ko writing
- Japanese Intro 11 - Quiz Hiragana/Katakana a i u e o reading 
- Japanese 11 - Quiz All/any hiragana writing/reading
- Japanese 9 - Quiz Hiragana a i u e o reading
