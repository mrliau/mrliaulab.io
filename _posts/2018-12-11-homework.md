---
layout: homework
title: Listen to This - Why Sleep Matters
date: 2018-12-11 01:00:00 -07:00
type: post
published: true
status: publish
categories:
- homework
---

<i>“The shorter your sleep, the shorter your life.” -Dr. Matthew Walker</i>

Think you can drive without sleeping? 
What's the ideal time for students to sleep?
How you are really affected by lack of sleep.

Find out this and more by listening to this:
<a href="http://www.jordanharbinger.com/matthew-walker-unlocking-the-power-of-sleep-and-dreams/">The Jordan Harbinger Shower - Matthew Walker | Unlocking the Power of Sleep and Dreams</a>





