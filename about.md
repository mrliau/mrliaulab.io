---
layout: page
title: About
permalink: /about/
---
<figure style="float:left;">
<img src="/assets/yukata.jpg" alt="Artwork by students" style="height: 300px; padding-right: 10px;">
<figcaption>Artwork by students</figcaption>
</figure>
TPRS Japanese Language Learning Lab is a collection of stories and resources for TPRS Japanese teaching. Mr Liau is a new TPRS Japanese teacher, and is experimenting and developing new content with the aid of students. Many materials are experimental and part of the learning process for new and lead learners. 

The most difficult limitation to overcome is often ourselves. We have literally spent our whole lives learning that things are the way that we think they are. If we can teach ourselves to be open to moving past those ideas, then, and only then, can we be ready to learn something new, and possibly better.

<hr />

In <a href="https://kwiklearning.com/">Jim Kwik's</a> <strong>F-A-S-T</strong> method of learning, the first step is to forget our preconceptions:

<p>&nbsp;</p>
<strong>F = Forget</strong>

"[Forget] about what you already know about the subject. I find as we grow older, we feel like we know everything<strong>,</strong> so we’re not learning anything new; we’re just looking to validate what we already know."[1]

<strong>A = Active</strong>

"Twentieth-century education trained us to be passive learners. We’re just sitting there, we’re supposed to consume information, but that’s not how the human mind really works." We must be active: ask questions, engage ourselves, and take notes whe appropriate.[2]

<strong>S = State</strong>

"We know this: all learning is state-depending. Whenever I want to learn fast a skill or a new subject, I always put myself in a state that’s conducive to what I want to learn. A state of curiosity, a state of fascination, a state of excitement."[3]

<strong>T = Teach</strong>

"If you had to watch a video or a training or read a book and you had to present it to someone the very next day, would you pay different levels of attention? [...] If I ever want to learn something really fast, a subject or a skill, I also put my professor’s cap on [...] All of a sudden I find that my retention of the information is twofold. [...] When you teach something to someone else, you get to learn it twice."[4]

[1][2][3] http://www.supernutritionacademy.com/reduce-overwhelm-get-focused-jim-kwik/#.VkQBrXoSVoA
<br/>[4] https://www.bulletproofexec.com/transcript-jim-kwik-speed-reading-memory-superlearning-189/

